
import { LightningElement, track, api, wire } from "lwc";
import { getRecord } from "lightning/uiRecordApi";
import { updateRecord } from 'lightning/uiRecordApi';
import USERID_FIELD from "@salesforce/schema/User.Id";
import LANGUAGELOCALEKEY_FIELD from "@salesforce/schema/User.LanguageLocaleKey";
import USER_ID from "@salesforce/user/Id";

export default class UserLanguage extends NavigationMixin(LightningElement) {
    @track locale;
	
    get languageOptions() {
        return [{
                label: "English",
                value: "en_US"
            },
            {
                label: "Spanish",
                value: "es"
            }
        ];
	}
    
    @wire(getRecord, {
		recordId: USER_ID,
		fields: [LANGUAGELOCALEKEY_FIELD]
	})
	wireuser({ error, data }) {
		if (error) {
			this.error = error;
		} else if (data) {
			this.locale = data.fields.LanguageLocaleKey.value;
		}
	}
    
    handleLanguageChange(e){
		this.locale = e.detail.value;
		this.setUserLanguageLocale(this.locale);
	}
    
    setUserLanguageLocale(languageLocaleKey) {
		let promises = [];
		let fields = {};
		fields[USERID_FIELD.fieldApiName] = USER_ID;
		fields[LANGUAGELOCALEKEY_FIELD.fieldApiName] = languageLocaleKey;
		promises.push(updateRecord({ fields }));
		
		Promise.all(promises)
        .then(() => {
			window.location.reload();
        })
        .catch(error => {
            console.log(error);
		});
	}
}